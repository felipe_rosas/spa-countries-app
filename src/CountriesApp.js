import React from 'react'
import { AppRouter } from './routes/AppRouter'

export const CountriesApp = () => {
    return (
        <div>
            <AppRouter/>
        </div>
    )
}
