import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import { Navbar } from '../components/ui/Navbar'
import { CountriesScreen } from '../components/countries/CountriesScreen';
import { RegionsScreen } from '../components/regions/RegionsScreen';

export const DashboardRoutes = () => {
    return (
        <>
            <Navbar/>

            <div>
                <Switch>
                    <Route exact path="/paises" component={ CountriesScreen } />
                    <Route exact path="/paises/:region" component={ CountriesScreen } />
                    <Route exact path="/regiones" component={ RegionsScreen } />

                    <Redirect to="/paises" />
                </Switch>
            </div>

        </>
    )
}
