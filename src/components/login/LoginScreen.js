import React from 'react'

export const LoginScreen = ({ history }) => {

    const handleClick = () => {
        history.push('/');
    }

    return (
        <div className="container mt-5">
            <h2>login</h2>
            <hr/>

            <button
            className="btn btn-primary"
            onClick={ handleClick }
            >
                Login
            </button>
        </div>
    )
}
